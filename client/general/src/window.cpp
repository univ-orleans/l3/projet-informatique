#include "window.hpp"

GtkWidget *tf_ip; // Champs pour l'IP de destination
GtkWidget *tf_port; // Champs pour le port de destination
GtkWidget *tf_nom; // Champs pour la clef
GtkWidget *tf_value; // Chmaps pour la valeur



void destroy_action( GtkWidget *widget,gpointer data )
{
   gtk_main_quit ();
}

void confirm_action(GtkWidget *widget, gpointer *data)
{
    cbor_item_t * root = cbor_new_definite_map(2); // Creation d'un paquet cbor

	const char * newNom = gtk_entry_get_text(GTK_ENTRY(tf_nom)); // Recuperation de la clef
	const char * newValue = gtk_entry_get_text(GTK_ENTRY(tf_value)); // Recuperation de la valeur
		
    cbor_pair val1 = {cbor_move(cbor_build_string(newNom)), cbor_move(cbor_build_string(newValue))};
    //~ cbor_pair val2 = {cbor_move(cbor_build_string("circle_size")), cbor_move(cbor_build_string("4"))};

	cbor_map_add(root, val1);
    //~ cbor_map_add(root, val2);
    
    sendUDPMessage(gtk_entry_get_text(GTK_ENTRY(tf_ip)), gtk_entry_get_text(GTK_ENTRY(tf_port)), root); // Envoie du message UDP
}

void run_window(int *argc, char ***argv)
{
    GtkWidget *window;

    GtkWidget *button;

    GtkWidget *l_ip;
    GtkWidget *l_port;
    GtkWidget *l_nom;
    GtkWidget *l_value;

    GtkWidget *layout;

    gtk_init(argc, argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width(GTK_CONTAINER(window), 20);

    layout = gtk_layout_new(NULL,NULL); // Pane

    tf_ip = gtk_entry_new();
    tf_port = gtk_entry_new();
    tf_nom = gtk_entry_new();
    tf_value = gtk_entry_new();

    gtk_entry_set_text(GTK_ENTRY(tf_ip), "127.0.0.1"); // IP par defaut dans le champs de texte tf_ip
    gtk_entry_set_text(GTK_ENTRY(tf_port), "6789"); // Port par defaut dans le champs de texte tf_port

    l_ip = gtk_label_new("IP de destination : ");
    l_port = gtk_label_new("Port de destination : ");
    l_nom = gtk_label_new("Nom : ");
    l_value = gtk_label_new("Valeur : ");

    button = gtk_button_new_with_label("Valider");

    g_signal_connect(window, "destroy", G_CALLBACK(destroy_action), NULL);
    
    g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(confirm_action), NULL);

    gtk_layout_put(GTK_LAYOUT(layout), l_ip, 0, 0);
    gtk_layout_put(GTK_LAYOUT(layout), tf_ip, 150, 0);
    gtk_layout_put(GTK_LAYOUT(layout), l_port, 0, 40);
    gtk_layout_put(GTK_LAYOUT(layout), tf_port, 150, 40);
    gtk_layout_put(GTK_LAYOUT(layout), l_nom, 0, 80);
    gtk_layout_put(GTK_LAYOUT(layout), tf_nom, 150, 80);
    gtk_layout_put(GTK_LAYOUT(layout), l_value, 0, 120);
    gtk_layout_put(GTK_LAYOUT(layout), tf_value, 150, 120);
    gtk_layout_put(GTK_LAYOUT(layout), button, 150, 180);

    gtk_layout_set_size(GTK_LAYOUT(layout), 100,100);
    gtk_container_add (GTK_CONTAINER (window), layout);

    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 500, 300); 
    gtk_window_set_title(GTK_WINDOW(window), "client.exe");
 
    gtk_widget_show_all(window); 
 
    gtk_main ();
}
