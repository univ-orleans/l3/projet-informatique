#include "socket.hpp"

int resolvehelper(const char* hostname, int family, const char* service, sockaddr_storage* pAddr)
{
    int result; // Resultat de différentes commande
    addrinfo* result_list = NULL;
    addrinfo hints = {};
    hints.ai_family = family; 
    hints.ai_socktype = SOCK_DGRAM; 
    result = getaddrinfo(hostname, service, &hints, &result_list); // Recuperation des formation du client
    if (result == 0)
    {
        memcpy(pAddr, result_list->ai_addr, result_list->ai_addrlen);
        freeaddrinfo(result_list);
    }

    return result;
}

void sendUDPMessage(const char * ip, const char * port, cbor_item_t * data)
{
    int result = 0;
    int sock = socket(AF_INET, SOCK_DGRAM, 0); // Utilisation d'une socket en UDP

    char szIP[100];

    sockaddr_in addrListen = {}; 
    addrListen.sin_family = AF_INET;
    result = bind(sock, (sockaddr*)&addrListen, sizeof(addrListen)); 
    if (result == -1)
    {
       int lasterror = errno;
       std::cout << "error: " << lasterror;
       exit(1);
    }

    sockaddr_storage addrDest = {}; // Adresse du destinataire
    result = resolvehelper(ip, AF_INET, port, &addrDest);
    if (result != 0)
    {
       int lasterror = errno;
       std::cout << "error: " << lasterror;
       exit(1);
    }

    unsigned char * buffer;
    size_t buffer_size;
    size_t length = cbor_serialize_alloc(data, &buffer, &buffer_size); // Cryptage des données à envoyer et recuperation de la taille
    
    fwrite(buffer, 1, length, stdout); //Ecriture des dannée dans le buffer
	       
    const unsigned char* msg = buffer;
    size_t msg_length = length;

    result = sendto(sock, msg, msg_length, 0, (sockaddr*)&addrDest, sizeof(addrDest)); // Envoie à l'adresse recepteur des données

    free(buffer);
    fflush(stdout);
    cbor_decref(&data);
}