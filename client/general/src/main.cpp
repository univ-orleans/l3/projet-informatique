
#include <iostream>
#include <cbor.h>
#include <gtk/gtk.h>

#include "window.hpp"

int main(int argc, char *argv[])
{
    
    run_window(&argc, &argv);
    
    return 0;

}
