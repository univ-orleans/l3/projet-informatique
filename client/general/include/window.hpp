/**
 * @file window.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Programme permettant l'affichage d'un formulaire afin d'envoyer un message UDP
 * @version 0.1
 * @date 2020-02-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <cbor.h>
#include <gtk/gtk.h>
#include <regex>

#include "socket.hpp"

/**
 * @brief Action du bouton de validation
 * 
 * @param widget 
 * @param data 
 */
void confirm_action(GtkWidget *widget, gpointer *data);

/**
 * @brief Action du bouton de fermeture de la fenêtre
 * 
 * @param widget 
 * @param data 
 */
void destroy_action( GtkWidget *widget,gpointer data );

/**
 * @brief Affichage du formulaire
 * 
 * @param argc 
 * @param argv 
 */
void run_window(int *argc, char ***argv);

#endif