/**
 * @file socket.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Programme permettant l'envoie d'un message sous forme de cbor_item à un serveur en UDP
 * @version 0.1
 * @date 2020-02-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <cbor.h>
#include <stdio.h>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <errno.h>
#include <stdlib.h>
#include <iostream>

/**
 * @brief Verification des informations du serveur
 * 
 * @param hostname
 * @param family 
 * @param service 
 * @param pAddr 
 * @return int 
 */
int resolvehelper(const char* hostname, int family, const char* service, sockaddr_storage* pAddr);

/**
 * @brief Envoie des données en UDP
 * 
 * @param ip 
 * @param port 
 * @param data 
 */
void sendUDPMessage(const char * ip, const char * port, cbor_item_t * data);

#endif