// INCLUDES
// ================================================================================
#include "socket.hpp"



// METHODS
// ================================================================================
/**
 * @brief Vérification d'une bonne configuration du client
 * 
 * @param addr_destination l'adresse de destination (serveur cible)
 * @return int 
 */
int Configuration(sockaddr_storage* addr_destination) {
    int result;
    addrinfo* result_list = NULL;
    addrinfo hints = {};
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM; 
    result = getaddrinfo(ADRESSE_IP, PORT, &hints, &result_list);
    
    if (result == 0) {
        memcpy(addr_destination, result_list->ai_addr, result_list->ai_addrlen);
        freeaddrinfo(result_list);
    }

    return result;
}



/**
 * @brief Envoie sur le réseau des datagrames (paquets en UDP).
 * 
 * @param data L'ensemble des données à envoyer sur le réseau
 */
void SendMessage(cbor_item_t* data) {
    cout << "pass" << endl;
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    char szIP[100];
    sockaddr_storage addr_destination = {};
    sockaddr_in addr_listen = {}; 
    addr_listen.sin_family = AF_INET;

    int result = bind(sock, (sockaddr*)&addr_listen, sizeof(addr_listen));
    if (result == -1) {
       int lasterror = errno;
       std::cout << "error: " << lasterror;
       exit(EXIT_FAILURE);
    }

    result = Configuration(&addr_destination);
    if (result != 0) {
       int lasterror = errno;
       std::cout << "error: " << lasterror;
       exit(EXIT_FAILURE);
    }

    unsigned char * buffer;
    size_t buffer_size;
    size_t length = cbor_serialize_alloc(data, &buffer, &buffer_size);
    
    fwrite(buffer, 1, length, stdout);
	       
    const unsigned char* msg = buffer;
    size_t msg_length = length;

    result = sendto(sock, msg, msg_length, 0, (sockaddr*)&addr_destination, sizeof(addr_destination));

    free(buffer);
    fflush(stdout);
    cbor_decref(&data);
}



/**
 * @brief Modifie les valeurs des "barres" du fichier SVG "music.svg".
 * Les barres sont au nombre de 3 et chaqu'une d'elle se voie attribuer
 * une valeur aléatoirement et indépendament des autres.
 * 
 */
void Music(){
    srand( time(NULL) );
	
    int value;
	int key_id;

    const int ITERATIONS = (UNE_SECONDE / INTERVAL_ENVOIE) * 60 * TEMPS_TOTAL;  // le nombre d'itération
	
    for(int i = 0; i < ITERATIONS; ++i) {
		
        cbor_item_t * data = cbor_new_definite_map(NOMBRE_BARRE * 2);  // l'ensemble des données à envoyer
        
        for (int key_id = 1; key_id <= NOMBRE_BARRE; key_id++) {
            value = rand() % (VALEUR_MAX + 1); // entre 0 et VALEURS_MAX inclus
            string key_str = "value_" + to_string(key_id);
            string value_str = to_string(VALEUR_MAX - value) + "%";

            // ajouté la paire (key, value) à l'ensembl des données à envoyer
            cbor_pair paire = {
                cbor_move(cbor_build_string( key_str.c_str() )),
                cbor_move(cbor_build_string( value_str.c_str() ))
            };
            cbor_map_add(data, paire);

        }

        SendMessage(data);
		usleep(INTERVAL_ENVOIE);
    }

}
