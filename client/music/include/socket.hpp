/**
 * @file socket.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Programme permettant l'envoie d'un message sous forme de cbor_item à un serveur en UDP
 * @version 0.2
 * @date 2020-02-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef SOCKET_HPP
#define SOCKET_HPP



// DEFINE
// ================================================================================
#define PORT "6789"
#define ADRESSE_IP "127.0.0.1"
#define NOMBRE_BARRE 3 // le nombre de barre présent dans le svg
#define VALEUR_MIN 0   // la valeur minimale est 0%
#define VALEUR_MAX 63  // la valeur maximale est 31%
#define INTERVAL_ENVOIE 500000  // temps entre chaque envoie en microsecondes (soit 0.5 secondes)
#define UNE_SECONDE 1000000 // en microsecondes
#define TEMPS_TOTAL 1  // temps total d'exécution du client en minutes (3 minutes)



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <time.h>
#include <memory.h>
#include <sys/socket.h>
#include <cbor.h>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>



// NAMESPACE
// ================================================================================
using namespace std;



// METHODS
// ================================================================================
int Configuration(sockaddr_storage* addr_destination);
void SendMessage(cbor_item_t* data);
void Music();

#endif
