var indexSectionsWithContent =
{
  0: "abcdefghijklmoprstvw~",
  1: "agps",
  2: "aglpst",
  3: "acdehijloprst~",
  4: "cdefkpsvw",
  5: "epst",
  6: "abdefmprstw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Macros"
};

