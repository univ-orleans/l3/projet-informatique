/**
 * @file affichage.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Classe de l'affichange graphique du serveur général.
 * @version 0.1
 * @date 2020-03-19
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef AFFICHAGE_HPP
#define AFFICHAGE_HPP



// DEFINE
// ================================================================================
#define WINDOW_DEFAULT_WIDTH 500
#define WINDOW_DEFAULT_HEIGHT 350
#define WINDOW_TITLE "GTK window"



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <map>
#include <string>
#include <tinyxml2.h>
#include <librsvg-2.0/librsvg/rsvg.h>
#include <gtk-3.0/gtk/gtk.h>
#include <cairo/cairo.h>

#include "module/log.hpp"
#include "module/gdata.hpp"



// NAMESPACE
// ================================================================================
using namespace tinyxml2;
using namespace std;



// CLASS
// ================================================================================
class Affichage {
    private:
        // ---------- Attributes
        struct global_data *g_data; /**< Structure globale. */
    

    public:
        // ---------- Constructors & Destructor
        /**
         * @brief Constructeur de l'objet Affichage.
         * 
         * @param g_data Structure global_data stockant les informations nécessaires à la construction de l'objet.
         */
        Affichage(struct global_data *g_data);

        /**
         * @brief Constructeur par copie de l'objet Affichage.
         * 
         * @param affichage Objet de type Affichage.
         */
        Affichage(const Affichage &affichage);

        /**
         * @brief Destructeur de l'objet Affichage.
         * 
         */
        ~Affichage();


        // ---------- Methods
        /**
         * @brief Initialisation de la fenêtre graphique.
         * 
         * Les arguments `argc` et `argv` sont utilisé pour initialisé le thread principal
         * de l'affichage graphique.
         * 
         * @param argc Le nombre de paramètre.
         * @param argv La liste des paramètres.
         */
        void InitWindow(int argc, char *argv[]);

        /**
         * @brief Dessine le document svg sur le canvas.
         * 
         */
        void DrawSvg();

        /**
         * @brief Démarre l'affichage de la fenêtre graphique.
         * 
         */
        void Start();

        /**
         * @brief Arrête l'affichage de la fenêtre graphique.
         * 
         */
        void Stop();


        // ---------- Static Methods
        /**
         * @brief Méthode d'événement.
         * 
         * Capture l'événement de mise à jour de la zone de dessin et appel la méthode
         * do_drawing_svg().
         * 
         * @param widget Un widget de dessin.
         * @param canvas Le canvas du widget.
         * @param user_data Le pointeur du `RsvgHandle` courant casté en `void*`.
         * @return gboolean 
         */
        static gboolean on_draw_event(GtkWidget *widget, cairo_t *canvas, gpointer user_data);

        /**
         * @brief Redessine le document svg sur le canvas.
         * 
         * La méthode appel DrawSvg() pour remettre à jour le `svg_handle` ainsi que
         * le `canvas` avant d'afficher ce dernier à l'écran.
         * 
         * @param canvas Le canvas courant.
         * @param svg_handle Le dessin produit à partir du document svg.
         */
        static void do_drawing_svg(cairo_t * canvas, RsvgHandle *svg_handle);

        /**
         * @brief Méthode d'événement.
         * 
         * Capture l'événement de fermeture de la fenêtre et appel la méthode
         * do_destroy_window().
         * 
         * @param widget La fenêtre qui doit être fermé.
         * @param user_data Le pointeur de l'affichage courant casté en `void*`.
         * @return gboolean 
         */
        static gboolean on_destroy_event(GtkWidget *widget, gpointer user_data);

        /**
         * @brief Ferme la fenêtre graphique.
         * 
         * @param affichage L'affichage courant.
         */
        static void do_destroy_window(Affichage *affichage);

        /**
         * @brief Méthode d'événement.
         * 
         * @param widget Le widget dont la taille est redimentionné.
         * @param allocation La nouvelle taille du widget.
         * @param user_data Le pointeur de l'affichage courant casté en `void*`.
         * @return gboolean 
         */
        static gboolean on_resize_event(GtkWidget *widget, GdkRectangle *allocation, gpointer user_data);

        /**
         * @brief Redimentionne la taille du fichier document svg.
         * 
         * @param affichage L'affichage courant.
         */
        static void do_resize_svg(Affichage *affichage);

        /**
         * @brief Cette méthode est rappelé à chaque tick d'horloge. Cela nous permet
         * d'appeler la méthode DrawSvg() avec une cadance réguliaire
         * et ainsi assurer le rafraichissement de l'affichage.
         * 
         * @param widget Un widget.
         * @param frame_clock L'horloge du widget.
         * @param user_data Le pointeur de l'affichage courant casté en `void*`.
         * @return gboolean 
         */
        static gboolean TickClock(GtkWidget *widget, GdkFrameClock *frame_clock, gpointer user_data);

};

#endif