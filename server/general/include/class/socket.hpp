/**
 * @file socket.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Classe de la socket du serveur général.
 * @version 0.1
 * @date 2020-03-20
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef SOCKET_HPP
#define SOCKET_HPP



// DEFINE
// ================================================================================
#define PORT 6789
#define BUFFER_SIZE 1500



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <map>
#include <thread>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <cbor.h>
#include <string>
#include <tinyxml2.h>
#include <librsvg-2.0/librsvg/rsvg.h>
#include <gtk-3.0/gtk/gtk.h>

#include "module/log.hpp"
#include "module/gdata.hpp"
#include "module/aritmetique.hpp"
#include "class/parsage.hpp"



// NAMESPACE
// ================================================================================
using namespace tinyxml2;
using namespace std;



// CLASS
// ================================================================================
class Socket {
    private:
        // ---------- Attributes
        bool listen;                /**< Indiquateur découte de la socket. */
        int socket_udp;             /**< La socket UDP. */
        sockaddr_in socket_address; /**< L'adresse de la socket. */
        pthread_t thread;           /**< Le thread d'écoute du réseau. */
        struct global_data *g_data; /**< Structure globale. */
        Parsage *parseur;           /**< Le parseur du serveur. */
    

    public:
        // ---------- Constructors & Destructor
        /**
         * @brief Constructeur de l'objet Socket.
         * 
         * @param g_data Structure global_data stockant les informations nécessaires à la construction de l'objet.
         * @param parseur Objet Parsage
         */
        Socket(struct global_data *g_data, Parsage *parseur);

        /**
         * @brief Constructeur par copie de l'objet Socket.
         * 
         * @param socket Objet de type Socket.
         */
        Socket(const Socket &socket);

        /**
         * @brief Destructeur de l'objet Socket.
         * 
         */
        ~Socket();


        // ---------- Methods
        /**
         * @brief Initialise la socket.
         * 
         */
        void InitSocket();

        /**
         * @brief Démarre l'écoute sur le réseau.
         * 
         */
        void Start();

        /**
         * @brief Arrête l'écoute sur le réseau.
         * 
         */
        void Stop();

        /**
         * @brief Méthode d'écoute du réseau.
         * 
         * Réceptionne une ou plusieurs informations transmises via le réseau et les
         * tranmettent au parseur pour modifier le document svg.
         * 
         */
        void Run();


        // ---------- Static Methods
        /**
         * @brief Thread principale de la socket.
         * 
         * Le thread est utilisé pour appeler la méthode Run().
         * 
         * @param ptr La socket courante casté en (void*).
         * @return NULL
         */
        static void * Thread(gpointer ptr);
};

#endif