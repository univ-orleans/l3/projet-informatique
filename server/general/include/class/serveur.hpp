/**
 * @file serveur.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Classe du serveur général.
 * @version 0.1
 * @date 2020-03-22
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef SERVEUR_HPP
#define SERVEUR_HPP



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <map>
#include <string>
#include <thread>
#include <tinyxml2.h>

#include "module/log.hpp"
#include "module/gdata.hpp"
#include "class/affichage.hpp"
#include "class/parsage.hpp"
#include "class/socket.hpp"



// NAMESPACE
// ================================================================================
using namespace tinyxml2;
using namespace std;



// CLASS
// ================================================================================
class Serveur {
    private:
        // ---------- Attributes
        struct global_data *g_data; /**< Structure globale. */
        Affichage *affichage;       /**< L'affichage du serveur */
        Socket *socket;             /**< Socket du serveur */
        Parsage *parseur;           /**< Parseur du serveur */
        pthread_t thread;           /**< Thread d'écoute d'érreur du serveur */
    

    public:
        // ---------- Constructors & Destructor
        /**
         * @brief Constructeur de l'objet Serveur.
         * 
         * @param filepath Le chemin vers la resource SVG.
         */
        Serveur(const char *filepath);

        /**
         * @brief Constructeur par copie de l'objet Serveur.
         * 
         * @param serveur Objet de type Serveur.
         */
        Serveur(const Serveur &serveur);

        /**
         * @brief Destructeur de l'objet Serveur.
         * 
         */
        ~Serveur();


        // ---------- Methods
        /**
         * @brief Initialisation du serveur.
         * 
         * Le serveur initialise le parseur, la socket et l'affichage.
         * Les arguments `argc` et `argv` sont utilisé pour initialisé l'affichage graphique.
         * 
         * @param argc Le nombre de paramètre.
         * @param argv La liste des paramètres.
         * @return int Le code d'érreur 0 pour pas d'erreur et 1 si il y a une erreur.
         * @see Parsage::InitParsage()
         * @see Socket::InitSocket()
         * @see Affichage::InitWindow()
         */
        int InitServeur(int argc, char *argv[]);

        /**
         * @brief Démarre le serveur.
         * 
         * Le démarage du serveur comprend également le démarage de l'affichage ainsi
         * que la socket.
         * 
         */
        void Start();

        /**
         * @brief Arrête le serveur.
         * 
         * Déclanche également l'arrêt de l'affichage et de la socket.
         * 
         */
        void Stop();

        /**
         * @brief Méthode d'écoute des erreurs.
         * 
         * Elle appel la méthode Stop() du serveur à la 
         * moindre détection d'érreur. Une erreur est détecté si la valeur de
         * `this->g_data->error` passe de 0 à 1.
         * 
         */
        void Run();


        // ---------- Static Methods
        /**
         * @brief Thread principale du serveur.
         * 
         * Le thread est utilisé pour appeler la méthode Run().
         * 
         * @param ptr Le serveur courant casté en (void*).
         * @return NULL
         */
        static void * Thread(gpointer ptr);
};

#endif