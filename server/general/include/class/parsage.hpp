/**
 * @file parsage.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Classe de parsage de document XML du serveur général.
 * @version 0.1
 * @date 2020-03-21
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef PARSAGE_HPP
#define PARSAGE_HPP



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <map>
#include <string>
#include <tinyxml2.h>

#include "module/log.hpp"
#include "module/gdata.hpp"
#include "module/typage.hpp"
#include "module/animation.hpp"



// NAMESPACE
// ================================================================================
using namespace tinyxml2;
using namespace std;



// CLASS
// ================================================================================
class Parsage {
    private:
        // ---------- Attributes
        XMLElement *svg_root;       /**< Balise racine du document de type svg. */
        struct global_data *g_data; /**< Structure globale. */
    

    public:
        // ---------- Constructors & Destructor
        /**
         * @brief Constructeur de l'objet Parsage.
         * 
         * @param g_data Structure global_data stockant les informations nécessaires à la construction de l'objet.
         */
        Parsage(struct global_data *g_data);

        /**
         * @brief Constructeur par copie de l'objet Parsage.
         * 
         * @param parsage Objet de type Parsage.
         */
        Parsage(const Parsage &parsage);

        /**
         * @brief Destructeur de l'objet Parsage.
         * 
         */
        ~Parsage();


        // ---------- Methods
        /**
         * @brief Initialisation du parseur.
         * 
         */
        void InitParsage();

        /**
         * @brief Parse une balise XML.
         * 
         * @param tag Un élément XML.
         */
        void ParseSvgTag(XMLElement *tag);

        /**
         * @brief Modifie les valeurs des attributs des éléments parent à une balise
         * driven.
         * 
         * Pour pouvoir modifier cette valeur, les méthodes SetAttribute() et
         * SetAttributeStyle() sont appelées en fonction de l'emplacement de l'attribut
         * à modifier.
         * 
         * Exemple:
         * Selont l'exemple ci dessous, l'attribut `key` correspond à la valeur
         * `c_fill` et value à la nouvelle valeur qui remplacera `red`.
         * ```xml
         * <circle cx="50" cy="50" r="50" fill="red">
         *     <driven target="fill" by="c_fill"/>
         * </circle>
         * ```
         * 
         * @param key L'identifiant de la nouvelle valeur.
         * @param value La nouvvelle valeur.
         */
        void SetDrivenParent(string key, string value);

        /**
         * @brief Modifie la valeur des attributs qui sont directement dans la balise.
         * 
         * Exemple:
         * Selont l'exemple ci dessous, l'attribut `key` correspond à la valeur
         * `c_fill` et target à l'attribut `fill` situé dans la balise parent.
         * ```xml
         * <circle cx="50" cy="50" r="50" fill="red">
         *     <driven target="fill" by="c_fill"/>
         * </circle>
         * ```
         * 
         * @param key L'identifiant de la nouvelle valeur.
         * @param driven La balise driven.
         * @param parent La balise parent au driven.
         * @param target L'attribut cible.
         */
        void SetAttribute(string key, XMLElement *driven, XMLElement *parent, string target);
        
        /**
         * @brief Modifie la valeur des attributs qui sont dans l'attribut "style".
         * 
         * Exemple:
         * Selont l'exemple ci dessous, l'attribut `key` correspond à la valeur
         * `c_fill` et target à l'attribut `fill` situé dans la balise parent.
         * ```xml
         * <circle cx="50" cy="50" r="50" style="fill:red">
         *     <driven target="fill" by="c_fill"/>
         * </circle>
         * ```
         * 
         * @param key L'identifiant de la nouvelle valeur.
         * @param driven La balise driven.
         * @param parent La balise parent au driven.
         * @param target L'attribut cible.
         */
        void SetAttributeStyle(string key, XMLElement *driven, XMLElement *parent, string target);
        
        /**
         * @brief Vérifie si la balise est un driven.
         * 
         * @param tag Un élément XML.
         * @return true La balise est un driven.
         * @return false La balise n'est pas un driven.
         */
        bool IsDriven(XMLElement *tag);

        /**
         * @brief Vérifie si le driven comporte une animation.
         * 
         * Pour pouvoir réaliser une animation, le driven doit avoir un attribut `delay`
         * en indiquant le temps de l'animation en millisecondes.
         * 
         * Exemple:
         * Réalisation d'une animation de 0.3 secondes.
         * ```xml
         * <driven target="r" by="rayon" delay="300"/>
         * ```
         * 
         * @param driven Un élément XML.
         * @return true Le driven comporte une animation.
         * @return false Le driven ne comporte pas d'animation.
         */
        bool HasAnimation(XMLElement *driven);

        /**
         * @brief Sépart le contenu de l'attribut "style".
         * 
         * Le contenu de l'attribut `style` est séparé dans un premier temps par le
         * séparateur ";" et dans un second temps par le séparateur ":". Ainsi la 
         * méthode retourne une map avec pour clé les attributs et pour valeur les
         * valeurs des attributs.
         * 
         * @param text Le contenu de l'attribut style.
         * @return map<string, string> Une map des attributs valeurs contenu dans l'attribut style.
         */
        map<string, string> Split(string text);

        /**
         * @brief Fusione les attributs et les valeurs initialement placé dans l'attribut "style".
         * 
         * @param attributes_splited La map des attributs et valeurs à placer dans l'attribut "style".
         * @return string Le nouveau contenu de l'attribut "style".
         */
        string Join(map<string, string> attributes_splited);


        // ---------- Static Methods
        /**
         * @brief Un thread pour la modification des parents des drivens.
         * 
         * Cette méthode exécute dans un thread la méthode SetDrivenParent().
         * 
         * @param user_data Une structure parsage_data casté en `void*`.
         * @return NULL
         */
        static void * ThreadSetDrivenParent(void *user_data);
};



// STRUCT
// ================================================================================
/**
 * @struct Structure utilisé pour transmettre les informations suivantes au thread
 * qui vas modifier les balises.
 * 
 */
struct parsage_data {
    Parsage *parseur;   /**< Parseur du serveur */
    string key;         /**< Identifiant de la nouvelle valeur */
    string value;       /**< Nouvelle valeur */
};

#endif