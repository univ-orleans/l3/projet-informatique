/**
 * @file typage.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Module de vérification de type.
 * @version 0.1
 * @date 2020-03-24
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef TYPAGE_HPP
#define TYPAGE_HPP



// DEFINE
// ================================================================================
#define ATTRIBUTE_TYPE "type"
#define ATTRIBUTE_MIN "min"
#define ATTRIBUTE_MAX "max"
#define TYPE_STRING "string"
#define TYPE_INTEGER "int"
#define TYPE_DOUBLE "double"



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <tinyxml2.h>

#include "module/log.hpp"



// NAMESPACE
// ================================================================================
using namespace tinyxml2;
using namespace std;



// METHODS
// ================================================================================

/**
 * @brief Méthode de conversion d'un string en integer
 * 
 * @param value La valeur en string
 * @return int Le resultat en integer
 */
int ToInteger(string value);



/**
 * @brief Méthode de conversion d'un string en double
 * 
 * @param value La valeur en string
 * @return int Le resultat en double
 */
double ToDouble(string value);



/**
 * @brief Détecter qu'une valeur est plus petite ou égale que la valeur de l'attribut max du driven
 * 
 * @param value La nouvelle valeur
 * @param driven Le driven courant
 * @return true si la valeur est plus petite ou égale que la valeur de l'attribut max du driven
 * @return false si la valeur est plus grande que la valeur de l'attribut max du driven
 */
bool IsSmallerOrEqualThan(string value, XMLElement *driven);



/**
 * @brief Détecter qu'une valeur est plus grande ou égale que la valeur de l'attribut min du driven
 * 
 * @param value La nouvelle valeur
 * @param driven Le driven courant
 * @return true si la valeur est plus grande ou égale que la valeur de l'attribut min du driven
 * @return false si la valeur est plus petite que la valeur de l'attribut min du driven
 */
bool IsTallerOrEqualThan(string value, XMLElement *driven);



/**
 * @brief vérifier que la valeur peut être casté dans un type précisé
 * 
 * Cette Méthode Permet de vérifier que la valeur peut être casté dans
 * le type précisé par l'attribut type du driven.
 * 
 * Exemple : 
 * ```xml
 * <rect width="12.5" height="31.25" x="43.75" y="25">
 *      <driven target="height" by="value_2" type="double" min="0" max="31.25"/>
 * </rect>
 * ```
 * Ici la Méthode va essayé de caster la valeur en double et retourne si c'est possible ou non.
 * 
 * @param value La nouvelle valeur
 * @param driven Le driven courant
 * @return true Si la valeur peut être casté dans le type précisé dans le driven.
 * @return falseSi la valeur ne peut pas être casté dans le type précisé dans le driven.
 */
bool CheckType(string value, XMLElement *driven);



/**
 * @brief Vérifie la taille d'une valeur
 * 
 * Cette Méthode permet de vérifier la taille d'une valeur par rapport à celle
 * présente dans le driven courant.
 * 
 * @param value La nouvelle valeur
 * @param driven Le driven courant
 * @return true si la taille d'une valeur par rapport à celle présent dans le driven correspond.
 * @return false la taille d'une valeur par rapport à celle présent dans le driven ne correspond pas.
 */
bool CheckSize(string value, XMLElement *driven);



/**
 * @brief Méthode de vérification
 * 
 * Permet de vérifier la taille et le type d'une valeur par rapport à celle
 * présente dans le driven courant.
 * 
 * @param value La nouvelle valeur
 * @param driven Le driven courant
 * @return true si la taille et le type d'une valeur par rapport à celle présent dans le driven correspond.
 * @return false si la taille et le type d'une valeur par rapport à celle présent dans le driven ne correspond pas.
 */
bool Check(string value, XMLElement *driven);

#endif