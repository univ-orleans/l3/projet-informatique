/**
 * @file animation.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Module de réalisation d'animation pour les valeurs numériques.
 * @version 0.1
 * @date 2020-03-25
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef ANIMATION_HPP
#define ANIMATION_HPP



// DEFINE
// ================================================================================
#define MILLISECONDE_PRECISION 10



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <chrono>
#include <thread>
#include <tinyxml2.h>



// NAMESPACE
// ================================================================================
using namespace tinyxml2;
using namespace std;



// METHODS
// ================================================================================

/**
 * @brief Méthode d'animation
 * 
 * La méthode permet d'animer la modification d'un attribut d'un élément de l'image SVG.
 * C'est à dire, si par exemple on décide de modifier la taille d'un cercle de 10 à 20,
 * la méthode va animer le changement de la taille en modifiant la taille petit à petit.
 * 
 * @param value La nouvelle valeur
 * @param target L'attribut à modifier dans le parent du driven.
 * @param driven Le driven courant
 * @param parent Le parent du driven
 */
void Animation(string value, string target, XMLElement *driven, XMLElement *parent);

#endif