/**
 * @file aritmetique.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Module de calcul d'espression arithmétique.
 * @version 0.1
 * @date 2020-03-25
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef ARITMETIQUE_HPP
#define ARITMETIQUE_HPP



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <regex>
#include <string>

#include "lib/exprtk.hpp"



// NAMESPACE
// ================================================================================
using namespace std;



// TYPEDEF
// ================================================================================
typedef double T;
typedef exprtk::symbol_table<T> symbol_table_t;
typedef exprtk::expression<T> expression_t;
typedef exprtk::parser<T> parser_t;



// METHODS
// ================================================================================

/**
 * @brief Méthode d'évaluation arithmétique
 * 
 * La méthode permet de traduire une expression arithmétique sous forme
 * de string en un résultat de l'expression.
 * Par exemple si on a "4 + 4" la fonction retournera "8.0"
 * 
 * @param value L'expression arithmétique
 * @return string Le résultat de l'expression
 */
string EvaluateArithmetic(string value);

#endif