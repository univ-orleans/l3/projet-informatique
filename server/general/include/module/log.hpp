/**
 * @file log.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Module de log de debogage.
 * @version 0.1
 * @date 2020-03-25
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef LOG_HPP
#define LOG_HPP



// DEFINE
// ================================================================================
#define DEBUG_INFO    true
#define DEBUG_SUCCESS true
#define DEBUG_WARNING true
#define DEBUG_ERROR   true
#define DEBUG_MARKER  true

#define STYLE_BOLD "\033[01;"
#define FOREGROUND_CYAN    "36m"
#define FOREGROUND_GREEN   "32m"
#define FOREGROUND_ORANGE  "33m"
#define FOREGROUND_RED     "31m"
#define FOREGROUND_GRAY   "90m"
#define FOREGROUND_MAGENTA "35m"
#define RESET "\033[00;m"



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>



// NAMESPACE
// ================================================================================
using namespace std;



// METHODS
// ================================================================================

/**
 * @brief méthode de création d'un log de type informatif
 * 
 * @param filename le fichier concerné
 * @param message le message
 */
void LogInfo(string filename, string message);



/**
 * @brief méthode de création d'un log de type succés
 * 
 * @param filename le fichier concerné
 * @param message le message
 */
void LogSuccess(string filename, string message);



/**
 * @brief méthode de création d'un log de type alerte
 * 
 * @param filename le fichier concerné
 * @param message le message
 */
void LogWarning(string filename, string message);



/**
 * @brief méthode de création d'un log de type erreur
 * 
 * @param filename le fichier concerné
 * @param message le message
 */
void LogError(string filename, string message);



/**
 * @brief méthode de création d'un log de type point de marquage
 * 
 * @param filename le fichier concerné
 * @param message le message
 */
void LogMarker(string filename, string message);

#endif