/**
 * @file gdata.hpp
 * @author Arnaud Orlay (arnaud.orlay@etu.univ-orleans.fr)
 * @author Erwan Aubry (erwan.aubry@etu.univ-orleans.fr)
 * @author Marion Juré (marion.jure@etu.univ-orleans.fr)
 * @brief Structure de donnée partagé avec tous les composants.
 * @version 0.1
 * @date 2020-03-20
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef GDATA_HPP
#define GDATA_HPP



// DEFINE
// ================================================================================
#define ERROR 1



// INCLUDES
// ================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <map>
#include <string>
#include <tinyxml2.h>
#include <librsvg-2.0/librsvg/rsvg.h>
#include <gtk-3.0/gtk/gtk.h>



// NAMESPACE
// ================================================================================
using namespace tinyxml2;
using namespace std;



// STRUCT
// ================================================================================
/**
 * @struct Structure des données globals
 * 
 */
struct global_data {
    const char *filepath;                       /**< Chemin d'accès à la resource SVG. */
    GtkWidget *window;                          /**< Fenêtre graphique. */
    GtkWidget *draw_area;                       /**< Zone de dessin. */
    RsvgHandle *svg_handle;                     /**< Dessin du document SVG. */
    XMLDocument *svg_document;                  /**< Représentation de la resource SVG en objet. */
    map<string, XMLElement*> *svg_driven;       /**< Tableau des identifiants et de driven. */
    map<string, string> *client_data;           /**< Tableau des identifiants et nouvelles valeurs transmises sur le réseau. */
    map<string, pthread_t*> *parseur_threads;   /**< Tableau des threads de parsage pour chaque identifiant. */
    int *error;                                 /**< Indicateur d'erreur. */
};

#endif