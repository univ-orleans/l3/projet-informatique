// INCLUDES
// ================================================================================
#include "class/affichage.hpp"



// METHODS - CLASS
// ================================================================================
Affichage::Affichage(struct global_data *g_data) {
    this->g_data = g_data;
}



Affichage::Affichage(const Affichage &affichage) {
    this->g_data = affichage.g_data;
}



Affichage::~Affichage() {}



void Affichage::InitWindow(int argc, char *argv[]) {
    LogInfo("affichage", "Initialisation de la fenêtre graphique.");

    // initialisation de la fenêtre et du thread graphique
    gtk_init(&argc, &argv);

    // initialisation des composant de la fenêtre
    this->g_data->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    this->g_data->draw_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(this->g_data->window), this->g_data->draw_area);

    // convertion du document SVG en un dessin
    this->DrawSvg();

    // configuration de la fenêtre
    XMLElement *svg = this->g_data->svg_document->FirstChildElement();
    gtk_window_set_position(GTK_WINDOW(this->g_data->window), GTK_WIN_POS_CENTER);
	gtk_window_set_title(GTK_WINDOW(this->g_data->window), WINDOW_TITLE);
	gtk_window_set_default_size(GTK_WINDOW(this->g_data->window), svg->IntAttribute("width", WINDOW_DEFAULT_WIDTH), svg->IntAttribute("height", WINDOW_DEFAULT_HEIGHT));
    gtk_widget_show_all(this->g_data->window);
    svg = NULL;

    // Exécution d'une fonction à chaque tick d'horloge
    gtk_widget_add_tick_callback(this->g_data->draw_area, &TickClock, (void*) this, NULL);

    // Actions liées aux signaux de la fenêtre, sourie, ...
    g_signal_connect(G_OBJECT(this->g_data->draw_area), "draw",          G_CALLBACK(on_draw_event),    (void*) this->g_data->svg_handle);
    g_signal_connect(G_OBJECT(this->g_data->draw_area), "size-allocate", G_CALLBACK(on_resize_event),  (void*) this);
    g_signal_connect(G_OBJECT(this->g_data->window),    "destroy",       G_CALLBACK(on_destroy_event), (void*) this);
}



void Affichage::Start() {
    LogSuccess("affichage", "Ouverture de la fenêtre graphique.");
    gtk_main();
}



void Affichage::Stop() {
    gtk_main_quit();
    LogSuccess("affichage", "Fermeture de la fenêtre graphique.");
}



void Affichage::DrawSvg() {
    if (this->g_data->svg_document != NULL) {
        // créer le dessin
        XMLPrinter *printer = new XMLPrinter(0, false, 0);
        this->g_data->svg_document->Print(printer);
        
        // si svg_handle n'est pas initialisé, ont l'initialise
        if (!this->g_data->svg_handle) {
            this->g_data->svg_handle = rsvg_handle_new_from_data((const unsigned char*) printer->CStr(), printer->CStrSize()-1, NULL);
        }

        // si il est initialisé, ont change juste la valeur pointé
        else {
            *this->g_data->svg_handle = * rsvg_handle_new_from_data((const unsigned char*) printer->CStr(), printer->CStrSize()-1, NULL);
        }
        
        delete printer;

        // dessiner le dessin dans la zone de dessin
        gtk_widget_queue_draw(this->g_data->draw_area);
    }
    else {
        LogWarning("affichage", "Le document SVG est NULL.");
    }
}



// METHODS - STATIC
// ================================================================================
gboolean Affichage::on_draw_event(GtkWidget *draw_area, cairo_t *canvas, gpointer user_data) {
	do_drawing_svg(canvas, (RsvgHandle*) user_data);
	return false;
}



void Affichage::do_drawing_svg(cairo_t *canvas, RsvgHandle *svg_handle) {
	if (canvas != NULL) {
        rsvg_handle_render_cairo(svg_handle, canvas);
    }
}



gboolean Affichage::TickClock(GtkWidget *widget, GdkFrameClock *frame_clock, gpointer user_data) {
    Affichage *affichage = (Affichage*) user_data;
    affichage->DrawSvg();
    return true;
}



gboolean Affichage::on_destroy_event(GtkWidget *widget, gpointer user_data) {
    do_destroy_window((Affichage*) user_data);
    return false;
}



void Affichage::do_destroy_window(Affichage *affichage) {
    *affichage->g_data->error = ERROR;
}



gboolean Affichage::on_resize_event(GtkWidget *widget, GdkRectangle *allocation, gpointer user_data) {
    do_resize_svg((Affichage*) user_data);
    return false;
}



void Affichage::do_resize_svg(Affichage *affichage) {
    XMLElement *svg = affichage->g_data->svg_document->FirstChildElement("svg");
    if (svg) {
        svg->SetAttribute("width", gtk_widget_get_allocated_width(affichage->g_data->draw_area));
        svg->SetAttribute("height", gtk_widget_get_allocated_height(affichage->g_data->draw_area));
    }
}