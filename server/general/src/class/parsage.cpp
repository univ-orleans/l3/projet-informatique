// INCLUDES
// ================================================================================
#include "class/parsage.hpp"



// METHODS - CLASS
// ================================================================================
Parsage::Parsage(struct global_data *g_data) {
    this->g_data = g_data;
}



Parsage::Parsage(const Parsage &parsage) {
    this->g_data = parsage.g_data;
}



Parsage::~Parsage() {}



void Parsage::InitParsage() {
    LogInfo("parsage", "Initialisation du parseur.");

    // chargement du fichier SVG
    Whitespace whitespaceMode = PRESERVE_WHITESPACE;
    this->g_data->svg_document = new XMLDocument(true, whitespaceMode);
    XMLError error = this->g_data->svg_document->LoadFile(this->g_data->filepath);

    if (!error) {
        LogSuccess("parsage", "Chargement en mémoire du fichier SVG.");

        // initialisation de la balise racine d'un document svg.
        this->svg_root = this->g_data->svg_document->FirstChildElement("svg");

        // parsage du document SVG
        this->ParseSvgTag(this->svg_root);
    }
    else {
        LogError("parsage", "Le fichier SVG ne peut être chargé en mémoire !");
        this->g_data->svg_document = NULL;
        *this->g_data->error = ERROR;
    }
}



void Parsage::ParseSvgTag(XMLElement *tag) {
    // si c'est une balise et non pas du texte
    if (tag) {

        // si la balise est un driven, ont l'enregistre
        if (this->IsDriven(tag)) {
            string driven_key = tag->Attribute("by");
            map<string, XMLElement*>::iterator iterator = this->g_data->svg_driven->find(driven_key);
            if (iterator != this->g_data->svg_driven->end()) {
                iterator->second = tag;
            }
            else {
                this->g_data->svg_driven->insert(pair<string, XMLElement*>(driven_key, tag));
            }

            // enregistrement d'un thread pour l'identifiant du driven
            this->g_data->parseur_threads->insert(pair<string, pthread_t*>(driven_key, new pthread_t()));
            LogInfo("parsage", "Création d'un thread de parsage pour l'identifiant: " + driven_key);
        }

        // si la balise n'est pas un driven et a une balise enfant, ont parse l'enfant
        else if (!tag->NoChildren()) {
            this->ParseSvgTag(tag->FirstChildElement());
        }

        // si la balise a une balise adjacente, ont parse la balise voisine
        XMLElement *tag_next = tag->NextSiblingElement();
        if (tag_next != NULL) {
            this->ParseSvgTag(tag_next);
        }
    }
}



void Parsage::SetDrivenParent(string key, string value) {
    XMLElement *driven = this->g_data->svg_driven->at(key);
    XMLElement *parent = driven->Parent()->ToElement();    
    string attribute_target = driven->Attribute("target");
    const char *target = parent->Attribute(attribute_target.c_str());

    // si le type et les options (min, max, ...) énoncé sont correctent, 
    // alors ont modifie la balise parent du driven
    if (Check(value, driven)) {
        
        // la balise possède une animation
        if (this->HasAnimation(driven)) {
            Animation(value, attribute_target, driven, parent);
        }

        // la balise ne possède pas d'animation
        else {
            if (target) { this->SetAttribute(key, driven, parent, attribute_target); }
            else { this->SetAttributeStyle(key, driven, parent, attribute_target); }
        }
    }
}



void Parsage::SetAttribute(string key, XMLElement *driven, XMLElement *parent, string target) {
    parent->SetAttribute(target.c_str(), this->g_data->client_data->at(key).c_str());
}



void Parsage::SetAttributeStyle(string key, XMLElement *driven, XMLElement *parent, string target) {
    string attribute = "style";
    
    // si l'attribut "style" n'existe pas ou est vide
    if (!parent->Attribute(attribute.c_str())) {
        string content = target;
        content.append(":").append(this->g_data->client_data->at(key)).append(";");
        parent->SetAttribute(attribute.c_str(), content.c_str());
    }

    // si l'attribut "style" possède d'autres valeurs
    else {
        string attribute_content = parent->Attribute(attribute.c_str());
        map<string, string> map_attribute_content = this->Split(attribute_content);
        map<string, string>::iterator iterator = map_attribute_content.find(target);
        
        if (iterator != map_attribute_content.end()) {
            iterator->second = this->g_data->client_data->at(key);
        }
        else {
            map_attribute_content.insert(pair<string, string>(target, this->g_data->client_data->at(key)));
        }
        string content = this->Join(map_attribute_content);
        parent->SetAttribute(attribute.c_str(), content.c_str());
    }
}



bool Parsage::IsDriven(XMLElement *tag) {
    bool is_driven = false;
    string name = tag->Name();

    if (name == "driven") {
        is_driven = true;
    }

    return is_driven;
}



bool Parsage::HasAnimation(XMLElement *driven) {
    bool has_animation = false;
    double delay = driven->DoubleAttribute("delay");

    if (delay) {
        has_animation = true;
    }

    return has_animation;
}



map<string, string> Parsage::Split(string text) {
    map<string, string> out = map<string, string>();
    const char separator_primary = ';';
    const char separator_secondary = ':';
    string tmp = text;
    unsigned int iterator = tmp.find(separator_primary);

    while (iterator < tmp.size()) {
        string item = tmp.substr(0, iterator);
        string key = item.substr(0, item.find(separator_secondary));
        string value = item.substr(item.find(separator_secondary)+1, item.size());
        out.insert(pair<string, string>(key, value));
        tmp = tmp.substr(iterator+1, tmp.size());
        iterator = tmp.find(separator_primary);
    }
    
    if (tmp.size() > 0) {
        string key = tmp.substr(0, tmp.find(separator_secondary));
        string value = tmp.substr(tmp.find(separator_secondary)+1, tmp.size());
        out.insert(pair<string, string>(key, value));
    }

    return out;
}



string Parsage::Join(map<string, string> attributes_splited) {
    string out = "";
    map<string, string>::iterator iterator;
    for (iterator=attributes_splited.begin(); iterator != attributes_splited.end(); iterator++) {
        out += iterator->first + ":" + iterator->second + ";";
    }

    return out;
}



// METHODS - STATIC
// ================================================================================
void * Parsage::ThreadSetDrivenParent(void *user_data) {
    struct parsage_data *p_data = (struct parsage_data *) user_data;
    p_data->parseur->SetDrivenParent(p_data->key, p_data->value);

    return NULL;
}