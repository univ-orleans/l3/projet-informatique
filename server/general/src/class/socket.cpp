// INCLUDES
// ================================================================================
#include "class/socket.hpp"



// METHODS - CLASS
// ================================================================================
Socket::Socket(struct global_data *g_data, Parsage *parseur) {
    this->g_data = g_data;
    this->parseur = parseur;
}



Socket::Socket(const Socket &socket) {
    this->g_data = socket.g_data;
    this->parseur = socket.parseur;
}



Socket::~Socket() {}



void Socket::InitSocket() {
    LogInfo("socket", "Initialisation de la socket.");

    // création d'une socket (ipv4, UDP)
    this->socket_udp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    // définition de l'adresse de la socket
    this->socket_address.sin_addr.s_addr = INADDR_ANY; // écoute sur toutes les interfaces
    this->socket_address.sin_port = htons(PORT);       // définit dans le fichier "socket.hpp"
    this->socket_address.sin_family = AF_INET;         // l'adresse est en ipv4

    // écoute de la socket sur le réseau
    this->listen = true;

    // vérifacation de la création de la socket
    if (bind(this->socket_udp, reinterpret_cast<sockaddr*>(&socket_address), sizeof(socket_address)) == 0) {
        LogSuccess("socket", "Création de la socket.");
    }
    else {
        LogError("socket", "Mauvaise création de la socket !");
        *this->g_data->error = ERROR;
    }
}



void Socket::Start() {
    LogSuccess("socket", "Démarrage de la socket.");
    this->listen = true;

    // exécution d'un thread pour la boucle locale
    pthread_create(&this->thread, NULL, Socket::Thread, (void*) this);
}



void Socket::Stop() {
    this->listen = false;

    // message de d'arrêt de l'écoute sur le réseau
    LogInfo("socket", "Fin de l'écoute sur le port " + to_string(PORT) + ".");

    int error = pthread_cancel(this->thread);

    if (!error) { LogSuccess("socket", "Arrêt de la socket."); }
    else { LogError("socket", "La socket ne c'est pas arrêté !"); }
}



void Socket::Run() {
    // message de démarage de l'écoute sur le réseau
    if (this->listen && !*this->g_data->error) {
        LogInfo("socket", "Début de l'écoute sur le port " + to_string(PORT) + " ...");

        // boucle locale de la socket
        while (this->listen && !*this->g_data->error) {
            // l'expéditeur (client)
            sockaddr_in client;
            socklen_t message_size = sizeof(client);
            
            // le buffer (limite de taille défini dans "socket.hpp")
            char buffer[BUFFER_SIZE] = {0};

            // vérification de la réception des données
            if (recvfrom(this->socket_udp, buffer, BUFFER_SIZE, 0, reinterpret_cast<sockaddr*>(&client), &message_size) > 0) {          

                // transformation des données en un cbor
                cbor_data source = (cbor_data) buffer;
                struct cbor_load_result result;
                cbor_item_t *item = cbor_load(source, 1024, &result);
                cbor_pair *data_map = cbor_map_handle(item);

                // assigner les données reçus à la map des données client
                for (size_t i = 0; i < cbor_map_size(item); i++) {            
                    size_t key_size = cbor_string_length(data_map[i].key);
                    size_t value_size = cbor_string_length(data_map[i].value);

                    char *key_raw = reinterpret_cast<char*>(cbor_string_handle(data_map[i].key));
                    char *value_raw = reinterpret_cast<char*>(cbor_string_handle(data_map[i].value));

                    char *key = new char[key_size + 1];
                    char *value = new char[value_size + 1];

                    memcpy(key, key_raw, key_size);
                    memcpy(value, value_raw, value_size);

                    key[key_size] = '\0';
                    value[value_size] = '\0';

                    string key_str(key);
                    string value_str = EvaluateArithmetic((string) value);

                    LogInfo("socket", "Réception de la donnée: " + key_str + "=\"" + (string)value + "\"");
                    
                    // si le client envoie un attribut présent dans le fichier SVG
                    map<string, XMLElement*>::iterator iterator_svg = this->g_data->svg_driven->find(key_str);
                    if (iterator_svg != this->g_data->svg_driven->end()) {
                        
                        map<string, string>::iterator iterator_client = this->g_data->client_data->find(key_str);
                        if (iterator_client != this->g_data->client_data->end()) {
                            iterator_client->second = value_str;
                        }
                        else {                    
                            this->g_data->client_data->insert(pair<string, string>(key_str, value_str));
                        }

                        // parsage du document SVG pour chaque valeurs reçus
                        struct parsage_data *p_data = new struct parsage_data();
                        p_data->parseur = this->parseur;
                        p_data->key = key_str;
                        p_data->value = value_str;

                        map<string, pthread_t*>::iterator iterator_parseur = this->g_data->parseur_threads->find(key_str);
                        if (iterator_parseur != this->g_data->parseur_threads->end()) {
                            pthread_cancel(*iterator_parseur->second);
                            
                            pthread_create(iterator_parseur->second, NULL, Parsage::ThreadSetDrivenParent, (void*) p_data);
                        }
                    }

                    // si l'attribut envoyé par le client n'est pas utilisé dans le fichier SVG
                    else {
                        LogWarning("socket", "L'attribut \"" + key_str + "\" n'est pas utilisé dans le fichier SVG.");
                    }
                }
            }
            else {
                LogError("socket", "Mauvaise reception de données !");
                this->Stop();
            }
        }
    }

    // si la socket ne doit pas écouter ou si une erreur est survenu
    else {
        *this->g_data->error = ERROR;
    }
}



// METHODS - STATIC
// ================================================================================
void * Socket::Thread(gpointer ptr) {
    Socket *socket = (Socket*) ptr;
    socket->Run();

    return NULL;
}