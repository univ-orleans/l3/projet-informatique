// INCLUDES
// ================================================================================
#include "class/serveur.hpp"



// METHODS - CLASS
// ================================================================================
Serveur::Serveur(const char *filepath) {
    this->g_data = new struct global_data();
    this->g_data->filepath = filepath;
    this->g_data->svg_driven = new map<string, XMLElement*>();
    this->g_data->client_data = new map<string, string>();
    this->g_data->parseur_threads = new map<string, pthread_t*>();
    this->g_data->error = new int(0);
    
    this->parseur = new Parsage(this->g_data);
    this->affichage = new Affichage(this->g_data);
    this->socket = new Socket(this->g_data, this->parseur);
}



Serveur::Serveur(const Serveur &serveur) {
    this->g_data = serveur.g_data;
    this->affichage = serveur.affichage;
    this->socket = serveur.socket;
    this->parseur = serveur.parseur;
}



Serveur::~Serveur() {
    delete this->g_data;
    delete this->affichage;
    delete this->socket;
    delete this->parseur;
}



int Serveur::InitServeur(int argc, char *argv[]) {
    LogInfo("serveur", "Initialisation du serveur.");

    // chargement du fichier SVG et parsage du contenu
    // initialise: svg_document & svg_driven
    this->parseur->InitParsage();

    // mise en place d'une socket
    if (!*this->g_data->error) {
        this->socket->InitSocket();
    }

    // création de la fenêtre
    // initialise: window, draw_area & svg_handle
    if (!*this->g_data->error) {
        this->affichage->InitWindow(argc, argv);
    }

    return *this->g_data->error;
}



void Serveur::Start() {
    if (!*this->g_data->error) {
        LogSuccess("serveur", "Démarrage du serveur.");

        // exécution d'un thread d'écoute des erreurs
        pthread_create(&this->thread, NULL, Thread, (void*) this);
    
        this->socket->Start();
        this->affichage->Start();
    }
    else {
        LogError("serveur", "Le serveur ne peut pas démarrer.");
    }
}



void Serveur::Stop() {
    this->socket->Stop();
    this->affichage->Stop();
    
    LogSuccess("serveur", "Arrêt du serveur.");
}



void Serveur::Run() {
    while (!*this->g_data->error) {/* ne rien faire */}
    this->Stop();
}



// METHODS - STATIC
// ================================================================================
void * Serveur::Thread(gpointer ptr) {
    Serveur *serveur = (Serveur*) ptr;
    serveur->Run();

    return NULL;
}