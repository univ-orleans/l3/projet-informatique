// INCLUDES
// ================================================================================
#include "class/serveur.hpp"



// METHODS
// ================================================================================
int main(int argc, char *argv[]) {


    // si un paramètre est passé en argument
    if (argc == 2) {        
        // récupération du paramètre (fichier SVG)
        const char *filepath = argv[argc-1];

        // création du serveur
        Serveur *serveur = new Serveur(filepath);
        int error = serveur->InitServeur(argc, argv);

        if (!error) { serveur->Start(); }

        delete serveur;
    }

    // aucun ou plus de 1 paramètre est passé en argument
    else {
        LogWarning("main", "Vous avez passé " + to_string(argc-1) + " pamarètre !");
    }

    return 0;
}