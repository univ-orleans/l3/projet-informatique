// INCLUDES
// ================================================================================
#include "module/typage.hpp"



// METHODS
// ================================================================================
int ToInteger(string value) {
    int out;

    try {
        out = stoi(value);
        LogSuccess("typage", "Convertion de \"" + value + "\" en un entier.");
    }
    catch (const invalid_argument &error) {
        LogWarning("typage", "\"" + value + "\" n'est pas convertible en un entier !");
    }

    return out;
}



double ToDouble(string value) {
    double out;
    const string comma = ",";
    const string point = ".";
    const unsigned int index_comma = value.find(comma);
    unsigned int index_point = value.find(point);
    const unsigned int last_index = value.size() - 1;

    // ont remplace la virgule par le point
    if (index_comma <= last_index) {
        value.replace(index_comma, 1, point);
        index_point = index_comma;
    }

    // si le séparateur est le premier charactère
    if (index_point == 0) { 
        value = "0" + value;
    }

    // si le séparateur est le dernier charactère
    else if (index_point == last_index) {
        value = value.substr(0, last_index);
    }

    try {
        out = stod(value);
        LogSuccess("typage", "Convertion de \"" + value + "\" en un double.");
    }
    catch (const invalid_argument &error) {
        LogWarning("typage", "\"" + value + "\" n'est pas convertible en double !");
    }

    return out;
}



bool IsSmallerOrEqualThan(string value, XMLElement *driven) {
    bool is_valid = true;
    string type = driven->Attribute(ATTRIBUTE_TYPE) ? driven->Attribute(ATTRIBUTE_TYPE) : "";
    string size_max = driven->Attribute(ATTRIBUTE_MAX) ? driven->Attribute(ATTRIBUTE_MAX) : "";

    // si l'attribut existe
    if (type.size() && size_max.size()) {

        // pour les nombres entier
        if (type == TYPE_INTEGER) {
            try { is_valid = ToInteger(value) <= ToInteger(size_max); }
            catch (const invalid_argument &error) { is_valid = false; }
        }

        // pour les nombres à virgule
        else if (type == TYPE_DOUBLE) {
            try { is_valid = ToDouble(value) <= ToDouble(size_max); }
            catch (const invalid_argument &error) { is_valid = false; }
        }

        // pour les chaînes de caractères
        else {
            is_valid = value.size() <= (unsigned int) ToInteger(size_max);
        }
        
        if (is_valid) { LogSuccess("typage", "\"" + value + "\" est plus petit ou égale à " + size_max + "."); }
        else { LogWarning("typage", "\"" + value + "\" est plus grand que " + size_max + "."); }
    }

    return is_valid;
}



bool IsTallerOrEqualThan(string value, XMLElement *driven) {
    bool is_valid = true;
    string type = driven->Attribute(ATTRIBUTE_TYPE) ? driven->Attribute(ATTRIBUTE_TYPE) : "";
    string size_min = driven->Attribute(ATTRIBUTE_MIN) ? driven->Attribute(ATTRIBUTE_MIN) : "";

    // si l'attribut existe
    if (type.size() && size_min.size()) {

        // pour les nombres entier
        if (type == TYPE_INTEGER) {
            try { is_valid = ToInteger(value) >= ToInteger(size_min); }
            catch (const invalid_argument &error) { is_valid = false; }
        }

        // pour les nombres à virgule
        else if (type == TYPE_DOUBLE) {
            try { is_valid = ToDouble(value) >= ToDouble(size_min); }
            catch (const invalid_argument &error) { is_valid = false; }
        }

        // pour les chaînes de caractères
        else {
            is_valid = value.size() >= (unsigned int) ToInteger(size_min);
        }

        if (is_valid) { LogSuccess("typage", "\"" + value + "\" est plus grand ou égale à " + size_min + "."); }
        else { LogWarning("typage", "\"" + value + "\" est plus petit que " + size_min + "."); }
    }

    return is_valid;
}



bool CheckType(string value, XMLElement *driven) {
    bool is_valid = true;
    string type = driven->Attribute(ATTRIBUTE_TYPE) ? driven->Attribute(ATTRIBUTE_TYPE) : "";

    // si l'attribut existe
    if (type.size()) {

        // pour les nombres entier
        if (type == TYPE_INTEGER) {
            try { ToInteger(value); }
            catch (const invalid_argument &error) { is_valid = false; }
        }

        // pour les nombres à virgule
        else if (type == TYPE_DOUBLE) {
            try { ToDouble(value); }
            catch (const invalid_argument &error) { is_valid = false; }
        }
    }

    return is_valid;
}



bool CheckSize(string value, XMLElement *driven) {
    return IsTallerOrEqualThan(value, driven) && IsSmallerOrEqualThan(value, driven);
}



bool Check(string value, XMLElement *driven) {
    return CheckType(value, driven) && CheckSize(value, driven);
}