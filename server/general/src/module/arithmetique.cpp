// INCLUDES
// ================================================================================
#include "module/aritmetique.hpp"



// METHODS
// ================================================================================
string EvaluateArithmetic(string value) {
    expression_t expression;
    parser_t parser;
    string out = value;
    
    if (parser.compile(value, expression)) {
        out = to_string(expression.value());
        out.replace(out.find(","), 1, ".");
    }

    return out;
}