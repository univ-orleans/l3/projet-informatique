// INCLUDES
// ================================================================================
#include "module/animation.hpp"



// METHODS - CLASS
// ================================================================================
void Animation(string value, string target, XMLElement *driven, XMLElement *parent) {
    double delay = driven->DoubleAttribute("delay"); // millisecondes
    double clock = 0;

    double new_value = stod(value);
    double old_value = parent->DoubleAttribute(target.c_str());
    double intermediate_value = old_value;
    double step = (new_value - old_value) / (delay / MILLISECONDE_PRECISION);
    string final_value = to_string(intermediate_value);

    while (clock < delay) {
        intermediate_value += step;
        final_value = to_string(intermediate_value);
        final_value.replace(final_value.find(","), 1, ".");
        parent->SetAttribute(target.c_str(), final_value.c_str());

        clock += MILLISECONDE_PRECISION;
        this_thread::sleep_for(chrono::milliseconds( MILLISECONDE_PRECISION ));
    }
}