// INCLUDES
// ================================================================================
#include "module/log.hpp"



// METHODS
// ================================================================================
void LogInfo(string filename, string message) {
    if (DEBUG_INFO) {
        cout << STYLE_BOLD << FOREGROUND_CYAN << "[INFO   ] " << RESET;
        cout << STYLE_BOLD << FOREGROUND_GRAY << "(" << filename << ") " << RESET;
        cout << message << RESET << endl;
    }
}



void LogSuccess(string filename, string message) {
    if (DEBUG_SUCCESS) {
        cout << STYLE_BOLD << FOREGROUND_GREEN << "[SUCCESS] " << RESET;
        cout << STYLE_BOLD << FOREGROUND_GRAY << "(" << filename << ") " << RESET;
        cout << message << endl;
    }
}



void LogWarning(string filename, string message) {
    if (DEBUG_WARNING) {
        cout << STYLE_BOLD << FOREGROUND_ORANGE << "[WARNING] " << RESET;
        cout << STYLE_BOLD << FOREGROUND_GRAY << "(" << filename << ") " << RESET;
        cout << message << endl;
    }
}



void LogError(string filename, string message) {
    if (DEBUG_ERROR) {
        cout << STYLE_BOLD << FOREGROUND_RED << "[ERROR  ] " << RESET;
        cout << STYLE_BOLD << FOREGROUND_GRAY << "(" << filename << ") " << RESET;
        cout << message << endl;
    }
}



void LogMarker(string filename, string message) {
    if (DEBUG_MARKER) {
        string separator = " ";
        for (int i= 14+filename.size()+message.size(); i < 80 ; i++) { separator.push_back('-'); }
        cout << STYLE_BOLD << FOREGROUND_MAGENTA << "[MARKER ] " << RESET;
        cout << STYLE_BOLD << FOREGROUND_GRAY << "(" << filename << ") " << RESET;
        cout << STYLE_BOLD << FOREGROUND_MAGENTA << message << separator << RESET << endl;
    }
}