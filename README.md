# Projet Informatique


**date:** janvier à mars 2020 <br/>
**sujet:** sujet.pdf ([lien](/resources/doc/sujet.pdf)) <br/>
**langage:** C++ 11 <br/>
**todo liste:** todo.md ([lien](/todo.md)) <br/>
___
<br/><br/>


## Objectifs
Deux principaux modules d'enseignement coexistent au sein de ce projet, avec notamment la programmation impérative en **C++** et le réseaux avec l'utilisation des **sockets**.

Par groupe de 3 à 4 étudiant, nous devons réaliser une application _(serveur)_ qui doit pouvoir écouter sur le réseaux les informations transmises par un ou plusieurs _clients_. A partir de ces donnés, le serveur modifie un fichier **SVG** préalablement chargé en mémoire pour ensuite l'afficher dans une fenêtre graphique.

![schéma d'organisation du projet](/resources/img/principe.png)
<br/><br/>


## Récupération du projet
```bash
$ git clone https://gitlab.com/licence-info/projet-informatique.git
$ cd projet-informatique

# compilation du serveur
$ make -C server/general

# compilation des clients
$ make -C client/general
$ make -C client/music
```
Pour plus d'informations sur les options de compilation, ce référer à la [documentation de compilation](/doc/compilation.md) du manuel utilisateur.
<br/><br/>


## Démonstrations
Pour les démonstrations, il est préférable d'exécuter le client et le serveur dans deux consoles différentes. Pour plus d'information, se référer à la [documentation client](/doc/client.md) et à la [documentation serveur](/doc/serveur.md).
```bash
# démo 1
$ ./client/general/bin/main                            # console 1
$ ./server/general/bin/main ./resources/svg/circle.svg # console 2

# démo 2
$ ./client/music/bin/main                             # console 1
$ ./server/general/bin/main ./resources/svg/music.svg # console 2
```
<br/><br/>


## Documentations
Manuel utilisateur: [lien](/doc/README.md)

Documentation technique: serveur général ( [HTML](/server/general/doc/html/index.html) | [PDF](/server/general/doc/latex/refman.pdf) )
<br/><br/>


## Liens utils
- C++ 11 ([cppreference](https://en.cppreference.com/))
- SVG ([W3]( https://www.w3.org/TR/SVG11/eltindex.html))
- gtk ([developer](https://developer.gnome.org/gtk3/3.24/))
- cairo ([developer](https://developer.gnome.org/cairo/stable/))
- rsvg ([developer](https://developer.gnome.org/rsvg/2.46/))
- tinyxml ([GitHub](http://leethomason.github.io/tinyxml2/))
- cbor ([GitHub](https://github.com/PJK/libcbor))
<br/><br/>


## Contributeurs
- Arnaud ORLAY (@arnorlay) : Licence 3 ingénierie informatique
- Erwan Aubry (@cookiehacker) : Licence 3 ingénierie informatique
- Marion Juré (@marionjure) : Licence 3 ingénierie informatique
