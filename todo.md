# Todo list


### Processus minimal
|     | responsable   | titre     | description                                             |
| --- | ------------- | --------- | ------------------------------------------------------- |
| [x] | @cookiehacker | socket    | Réaliser le serveur et le client.                       |
| [x] | @marionjure   | affichage | Faire la fenetre d'affichage et les méthodes de dessin. |
| [x] | @arnorlay     | parsage   | Implémenter les méthodes de parsage du fichier SVG.     |


### Améliorations possibles
|     | responsable   | titre                            | description                                         |
| --- | ------------- | -------------------------------- | --------------------------------------------------- |
| [X] | @marionjure   | gestion des types                | Ignorer les valeurs du mauvais type, min et max.    |
| [x] | @arnorlay     | gestion des attributs composites | gestion de l'attribut "style".                      |
| [x] | @arnorlay     | animation                        | Réaliser une transition pour les valeures numérique |
| [x] | @cookiehacker | valeurs calculées                | Donner la possibilité de faire des opérations       |
