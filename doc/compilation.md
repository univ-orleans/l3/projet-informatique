# Compilation


Pour pouvoir compiler et/ou exécuter le(s) client(s), et le serveur, il vous faut vous assurer d'avoir installé Make. 

**Chaque client et serveur possède l'arborescence suivante:**

![schéma d'organisation du projet](/resources/img/arborescence.png)
<br/><br/>


## Comment compiler ?
Il est possible de compiler, exécuter ou nettoyer les différents répertoires du projet. vous devez donc utiliser la commande `make` et renseigner les options et le répertoire cible.
```bash
$ make [options] -C <repertoire_cible>
```

| Options | description                                                      |
| ------- | ---------------------------------------------------------------- |
| run     | exécuter le programme du répertoire cible                        |
| clean   | nettoyer le répertoire cible                                     |
| doc     | génère la documentation fonctionnel en Latex et HTML de la cible |

<br/><br/>


#### Quelques exemples
```bash
# compile le client "general"
$ make -C client/general

# compile le serveur "general"
$ make -C serveur/general

# exécute le client "general"
$ make run -C client/general

# nettoie le répertoire "general"
$ make clean -C client/general
```
<br/><br/>



---
[Accueil](/README.md) | [Documentation utilisateur](/doc/README.md)
