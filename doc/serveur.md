# Serveur


Le serveur est le cœur du projet. Le seul serveur disponible est dit "général" puisqu'il n'a pas été conçu pour un type d'affichage en particulier.
<br/><br/>


## Compilation
Pour la compilation du serveur, référez vous à la documentation sur la compilation ([lien](/doc/compilation.md)).
<br/><br/>


## Exécution
Pour exécuter le serveur il faut utiliser son exécutable `main` et donner en paramètre une ressousrce d'affichage au format SVG.
```bash
$ cd projet-informatique
$ ./serveur/general/bin/main <ressource_svg>
```
Deux ressources de démonstration sont disponibles. Elles peuvent être utilisées de la sort:
```bash
# affichage circle: sans animation
$ ./serveur/general/bin/main ./resources/svg/circle.svg

# affichage music: avec animation
$ ./serveur/general/bin/main ./resources/svg/music.svg
```
<br/><br/>


## Logs
Lors de l'exécution du serveur, les logs du serveur apparaîtront dans la sortie standard de la console. Pour les activer ou les désactiver, il suffit de modifier le fichier `serveur/general/include/module/log.hpp`
<br/><br/>



---
[Accueil](/README.md) | [Documentation utilisateur](/doc/README.md)
