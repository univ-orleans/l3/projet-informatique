# Client


Pour tester le projet ou plus précisément le serveur, il est possible d'utiliser ses propres clients ou les deux clients de démonstrations suivants.
<br/><br/>


## Clients de démonstration
#### Premier client: general
Le client nommé "general" est une interface graphique permettant l'envoie de données sur le réseau sans tenir compte de la ressource SVG utilisée par le serveur. Son interface ne permet cependant que l'envoi une à une des données.

![capture de l'interface graphique du client "general".](/resources/img/capture-interface-client-general.png)

La valeur dans le champs `Nom` doit correspondre à la valeur définie dans l'attribut `by` du driven. Si cela n'est pas respecté, le serveur ignorera les données transmises.
```xml
<circle x="0" y="0" r="20" fill="red">
    <driven target="fill" by="circle_fill">
</circle>
```
En se basant sur l'exemple de code ci-dessus, et sur l'affichage de l'interface du client, le cercle initialement rouge pourra être changé en bleu.
<br/>


#### Second client: music
Contrairement au premier client, celui-ci ne possède pas d'interface graphique ou terminale pour interagir avec l'utilisateur. Il est donc complètement autonome. En contre partie, il ne s'adapte donc pas aux ressources SVG. Il a été conçu pour la ressource *music.svg* ([lien](/resources/svg/music.svg)) afin de l'alimenter toutes les 0.5 secondes en envoyant 3 données simultanément.
<br/><br/>


## Utilisation
#### Compilation
Pour la compilation des différents clients, référez vous à la documentation sur la compilation [ici](/doc/compilation.md).
<br/>


#### Exécution
Pour l'exécution des clients, il existe deux méthodes. La première est énoncée les options de compilation [ici](/doc/compilation.md). La seconde méthode consiste à exécuter directement les exécutables des clients.
```bash
$ cd projet-informatique/client
$ ./<repertoire_client>/bin/main
```
A noter que `<repertoire_client>` est à remplacer par le répertoire ̀`general` ou `music`.
<br/><br/>



---
[Accueil](/README.md) | [Documentation utilisateur](/doc/README.md)
