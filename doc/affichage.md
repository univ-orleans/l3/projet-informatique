# Affichage


Un affichage est une ressource au format SVG qui est chargée par le serveur et affichée dans une fenêtre graphique. L'objectif de ces affichages est d'intégrer des éléments qui pourrons être modifier par les valeurs que reçoit le serveur. De ce fait, il est possible de concevoir une multitude d'image SVG. Pour la concrétisation de ce projet, noue avons décidé de nous concentrer sur deux types d'affichage.
<br/><br/>


## Affichages de démonstration
#### Premier affichage: circle
Le premier fichier SVG est un cercle rouge. Très simple à mettre en place et compréhensible par les collègues qui n'avaient jamais édité un fichier SVG. Il représente donc une bonne base de départ.

![capture de l'affichage de circle.svg](/resources/img/capture-affichage-ressource-circle.png)

Nous avons dans un premier temps ajouté un driven pour modifier la couleur. Ce fut un succès, nous avons continué dans cette lancée. Nous avons essayé de modifier le rayon du cercle et de le déplacer. Il est donc possible de faires varier les paramètres suivants:

| identifiant   | description                                |
| ------------- | ------------------------------------------ |
| `circle_fill` | Changer la couleur du cercle.              |
| `circle_size` | Changer la taille du cercle.               |
| `circle_cy`   | Changer la position du cercle sur l'axe Y. |
| `circle_op`   | Changer la l'opacité du cercle.            |

<br/>


#### Second affichage: music
Le second affichage a été édité principalement pour tester les capacitées à réaliser une animation rapide et fluide.

![capture de l'affichage de music.svg](/resources/img/capture-affichage-ressource-music.png)

Chaque barre verticale correspond à une valeur pour faire varier leurs taille aléatoirement.

| identifiant | description                                        |
| ----------- | -------------------------------------------------- |
| `value_1`   | Change le hauteur de la barre verticale de gauche. |
| `value_2`   | Change le hauteur de la barre verticale du centre. |
| `value_3`   | Change le hauteur de la barre verticale de droite. |

<br/><br/>


## Construction d'un affichage
L'affichage est une image SVG. Pour modifier dynamiquement les propriétées des balises XML, il faut ajouter une balise **driven**. Le driven est de la forme suivante:
```xml
<driven target="" by=""/>
```
Les balises driven s'appliquent uniquement à leur parent directe et chaque driven correspond à une propriété (ou attribut). Par conséquent une balise XML peut contenir plusieurs driven.

La valeur du `target` doit être une propriété du parent et le `by` doit contenir un identifiant.

Après cela, il est possible d'ajouter des options au driven.
- Pour définir un **type de donnée**, voir la documentation des améliorations dans la section typage [ici](/doc/ameliorations.md#typage).
- Pour définir des **bornes** min et max, ce référer à la documentation sur les améliorations dans la section restriction [ici](/doc/ameliorations.md#restrictions).
- Pour réaliser une **animation linéaire**, consulter la documentation sur les améliorations dans la section animation [ici](/doc/ameliorations.md#animation).
<br/><br/>



---
[Accueil](/README.md) | [Documentation utilisateur](/doc/README.md)
