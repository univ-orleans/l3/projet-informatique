# Documentation utilisateur
<br/><br/>


## Table de matières
- [Prerequis](/doc/prerequis.md)
  - [Installation des packages](/doc/prerequis.md#installation-des-packages)

- [Compilation](/doc/compilation.md)
  - [Comment compiler ?](/doc/compilation.md#comment-compiler-)
    - [Quelques exemples](/doc/compilation.md#quelques-exemples)

- [Serveur](/doc/serveur.md)
  - [Compilation](/doc/serveur.md#compilation)
  - [Exécution](/doc/serveur.md#exécution)
  - [Logs](/doc/serveur.md#logs)

- [Client](/doc/client.md)
  - [Clients de démonstration](/doc/client.md#clients-de-démonstration)
    - [Premier client: general](/doc/client.md#premier-client-general)
    - [Second client: music](/doc/client.md##second-client-music)
  - [Utilisation](/doc/client.md#utilisation)
    - [Compilation](/doc/client.md#compilation)
    - [Exécution](/doc/client.md#exécution)

- [Améliorations](/doc/ameliorations.md)
  - [Typage](/doc/ameliorations.md#typage)
  - [Restrictions](/doc/ameliorations.md#restrictions)
  - [Gestion de l'attribut style](/doc/ameliorations.md#gestion-de-lattribut-style)
  - [Animation](/doc/ameliorations.md#animation)
  - [Valeurs calculées](/doc/ameliorations.md#valeurs-calculées)
    - [Avantage de la solution](/doc/ameliorations.md#avantage-de-la-solution)
    - [Inconvénien de la solution](/doc/ameliorations.md#inconvénien-de-la-solution)

- [Affichage](/doc/affichage.md)
  - [Affichages de demonstration](/doc/affichage.md#affichages-de-démonstration)
    - [Premier affichage: circle](/doc/affichage.md#premier-affichage-circle)
    - [Second affichage: music](/doc/affichage.md#second-affichage-music)
  - [Construction d'un affichage](/doc/affichage.md#construction-dun-affichage)
<br/><br/>



---
[Accueil](/README.md)