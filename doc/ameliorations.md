# Améliorations
<br/><br/>


[1]: typage
## Typage
Il est possible de définir un type de donnée parmi **int**, **double** et **string**. La définition de ce type permet d'assurer en quelque sorte la concordance entre la valeur attendue et la valeur reçus.

Ainsi l'ajout d'un attribut `type` dans un driven permet d'activer cette vérification pour les données à destination de ce dernier.

Exemple:
```xml
<driven target="fill" by="couleur" type="string">
<driven target="cy" by="pos_y" type="int">
<driven target="fill-opacity" by="opacite" type="double">
```
<br/><br/>


[2]: restrictions
## Restrictions
Pour pouvoir mettre en place une restriction, il faut **impérativement définir un type** au préalable. Vous référez à la section type des améliorations [ici][1].

Une fois le typage défini, il est donc possible de borner la valeur attendue entre un **min** et un **max**. Pour se faire, il faut rajouter un attribut `min` si la valeur attendue doit être supérieur ou égale à une valeur précise et l'attribut `max` si elle doit être inférieur ou égale à un maximum.

Exemple:
```xml
<driven target="cy" by="pos_y" type="int" min="10">
<driven target="fill-opacity" by="opacite" type="double" min="0" max="1">
```
<br/><br/>


## Gestion de l’attribut style
L'attribut **style** est un attribut particulier. En effet, il a la possibilité d'avoir pour valeur un ensemble de pair tel que ces pairs sont constituées en clé d'un attribut et en valeur de la valeur associée. Ainsi, la propriété `fill="red"` peut s'écrire `style="fill:red;"`.

Le contenu de l'attribut style est un ensemble de propriétés séparé par un point-virgule et chaque propriété possède un attribut et une valeur. À partir de cette chaîne de caractère, nous récupérons les attributs et leur valeur associée pour former un tableau. Par la suite, il suffit de modifier la valeur de l'attribut correspondant, de reformer une chaîne de caractère et de remplacer le contenu de l'attribut style par cette nouvelle chaîne caractère.
<br/><br/>


## Animation
Les animations ne peuvent s'appliquer qu'uniquement aux valeurs numériques. Pour réaliser une animation linéaire, il faut ajouter l'attribut `delay` au driven. La valeur de cet attribut **s'exprime en millisecondes**.
```xml
<circle x="0" y="0" r="50">
    <driven target="r" by="rayon" delay="300">
</circle>
```
La balise driven doit appliquer une animation de transition sur la taille du cercle. L'animation a une durée totale de 0.3 secondes.
<br/><br/>


## Valeurs calculées
Pour pouvoir interpréter les expressions arithmétiques envoyées par les clients, nous utilisons la librairie [exprtk](http://www.partow.net/programming/exprtk/index.html) qui permet de résoudre les points suivants:
- Opérations arithmétiques
- Fonctions flottantes (log, fonctions trigonométriques)
- Fonctions tel que le min, max, abs, sign
- Conditionnelles

Les données réceptionnées passent d'abord par la résolution d'expression. Seul le résultat est utilisé lors du parsage du document SVG.
<br/>


#### Avantage de la solution
- La librairie est très simple d'utilisation
- Le parsage est efficace et rapide
- On obtient un grand nombre de fonctionnalités
<br/>


#### Inconvénien de la solution
- Ralentir énormément la compilation du serveur
<br/><br/>



---
[Accueil](/README.md) | [Documentation utilisateur](/doc/README.md)
