# Prérequis


Avant d'exécuter le projet, assurez vous que les librairies suivantes soient installées sur votre machine. Si toutefois ce n'est pas le cas, veuillez consulter la section sur [l'installation des packages][1] présente ci-dessous.

| Librairie | Package | version | Site de référence |
|-----------|---------|---------|-------------------|
|  gtk 3 | `libgtk-3-dev` | 3.24.12 | [pkgs.org](https://pkgs.org/download/libgtk-3-dev) |
|  cairo | `libcairo2-dev` | 1.16.0 | [pkgs.org](https://pkgs.org/download/libcairo2-dev) |
|  rsvg | `librsvg2-dev` | 2.40.20 | [pkgs.org](https://pkgs.org/download/librsvg2-dev) |
|  tinyxml 2 | `libtinyxml2-dev` | 7.0.0 | [pkgs.org](https://pkgs.org/download/libtinyxml2-dev) |
|  cbor | `libcbor-dev` | 0.5.0 | [pkgs.org](https://pkgs.org/download/libcbor-dev) |

> :warning: Les librairies installées doivent être supérieur ou égale au numéro de version précisé dans le tableau.
<br/><br/>


[1]: installation-des-packages
## Installation des packages
Pour installer les packages nécessaires à l'exécution du projet, vous devez ouvrir une console (aussi appelé shell ou terminal). Les commandes d'installation nécessitent de passer en mode super utilisateur `root` sur votre machine. Un mot de passe root vous sera demandé.
```bash
$ sudo su -
```

L'installation des packages passe par la commande `aptitude` comme suite:
```bash
$ apt install <nom_packages>
```
Pour rappel, l'utilisation des chevrons inférieur `<` et supérieur `>` indiques que l'élément à l'intérieur est à remplacer. Dans notre cas, il faudra donc remplacer `<nom_package>` par les packages des librairies présentées dans le tableau ci-dessus.
<br/><br/>



---
[Accueil](/README.md) | [Documentation utilisateur](/doc/README.md)
